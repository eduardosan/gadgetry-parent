<?php
if (!function_exists('tfuse_list_page_options')) :
    function tfuse_list_page_options() {
        $pages = get_posts( array(
            'post_type'      => 'page',
            'fields'         => 'ids',
            'posts_per_page' => 500,
            'orderby'        => 'title',
            'order'          => 'ASC',
        ) );

        $result    = array();
        $result[0] = 'Select a page';
        foreach ( $pages as $page ) {
            $result[ $page ] = get_the_title( $page );
        }

        return $result;
    }
endif;
